using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PositionHandler : MonoBehaviour
{
    public List<CarLapCounter> carLapCounters = new List<CarLapCounter>();

    void Start()
    {
        CarLapCounter[] carLapCountersArray = FindObjectsOfType<CarLapCounter>();

        carLapCounters = carLapCountersArray.ToList<CarLapCounter>();

        //hook up the pased checkpoint event
        foreach (CarLapCounter lapCounters in carLapCounters)
            lapCounters.OnPassCheckpoint += OnPassCheckpoint;
        
    }

    void OnPassCheckpoint(CarLapCounter carLapCounter)
    {
        //sort the cars position first based on how many checkpoints they have passsed, more is always better. then sort on time where shorter time is better
        carLapCounters = carLapCounters.OrderByDescending(s => s.GetNumberOfCheckpointsPassed()).ThenBy(s => s.GetTimeAtLastCheckpoint()).ToList();
       
        //get the cars position
        int carPosition = carLapCounters.IndexOf(carLapCounter) + 1;
       
        //tell the lap counter which position the car has
        carLapCounter.SetCarPosition(carPosition);
    }
     
}
