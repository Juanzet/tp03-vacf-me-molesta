using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Dialogues : MonoBehaviour
{
    [SerializeField, TextArea(4,6)] string[] dialogueLines;
    [SerializeField] float typingTime;
    [SerializeField] TMP_Text txt;
    Mesh mesh;
    Vector3[] vertices;
    int lineIndex;
    [SerializeField] float timeToText = 4;
    [SerializeField] float _cos;
    [SerializeField] float _sin;

    void Awake() 
    {
        //txt = GetComponent<TMP_Text>();
        StartCoroutine(ShowLine());
    }

    void Update()
    {
        //StartCoroutine(ShowLine());
        TextMeshProEffect();
        DialogueExplain();
    }

    void DialogueExplain()
    {
        //TextMeshProEffect();
        //StartCoroutine(ShowLine());
        timeToText -= Time.deltaTime;
        
        if(timeToText <= 0)
        {
            Destroy(txt);
        }

    }
    void TextMeshProEffect()
    {
        txt.ForceMeshUpdate();
        mesh = txt.mesh;
        vertices = mesh.vertices;
        
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 offset = Wobble(Time.time + i);

            vertices[i] = vertices[i] + offset;
        }
        //StartCoroutine(ShowLine());
        mesh.vertices = vertices;
        txt.canvasRenderer.SetMesh(mesh);
    }
    IEnumerator ShowLine()
    {
        txt.text = string.Empty;

        foreach (char c in dialogueLines[lineIndex])
        {
            txt.text += c;
            yield return new WaitForSeconds(typingTime);
        }
    }
    Vector2 Wobble(float time) 
    {
        //sin 3.3f
        //cos 2.5f
        return new Vector2(Mathf.Sin(time*_sin), Mathf.Cos(time*_cos));
    }
    public void Play()
    {
        SceneManager.LoadScene("Gameplay01");
    }
}
