using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class CarLapCounter : MonoBehaviour
{
    public TMP_Text carPositionText;
    int passedCheckPointNumber = 0;
    float timeAtLastPassedCheckPoint = 0;

    int numberOfPassedCheckpoints = 0;
    int lapsCompleted = 0;
    const int LAPS_TO_COMPLETE = 5;
    bool isRaceCompleted = false;
    int carPosition = 0;

    bool isHideRoutineRunning = false;
    float hideUIDelayTime;

    //events
    public event Action<CarLapCounter> OnPassCheckpoint;

    public void SetCarPosition(int pos)
    {
        carPosition  = pos;
    }

    public int GetNumberOfCheckpointsPassed()
    {
        return numberOfPassedCheckpoints;
    }

    public float GetTimeAtLastCheckpoint()
    {
        return timeAtLastPassedCheckPoint;
    }
    IEnumerator ShowPositionCO(float delayUntilHidePosition)
    {
        hideUIDelayTime += delayUntilHidePosition;

        carPositionText.text = carPosition.ToString();
        carPositionText.gameObject.SetActive(true);

        if(!isHideRoutineRunning)
        {
            isHideRoutineRunning = true;
            yield return new WaitForSeconds(hideUIDelayTime);
            carPositionText.gameObject.SetActive(false);
            isHideRoutineRunning = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.CompareTag("CheckPoint"))
        {
            if(isRaceCompleted)
                return;

            Checkpoint checkpoint = other.GetComponent<Checkpoint>();
            
            if(passedCheckPointNumber + 1 == checkpoint.checkPointNumber)
            {
                passedCheckPointNumber = checkpoint.checkPointNumber;
                numberOfPassedCheckpoints++;

                timeAtLastPassedCheckPoint = Time.time;

                if(checkpoint.isFinishLine)
                {
                    passedCheckPointNumber = 0;
                    lapsCompleted++;

                    if(lapsCompleted >= LAPS_TO_COMPLETE)
                        isRaceCompleted = true;
                }

                //Invoke the passed checkpoint event
                OnPassCheckpoint?.Invoke(this);

                if(isRaceCompleted)
                    StartCoroutine(ShowPositionCO(100));
                else StartCoroutine(ShowPositionCO(1.5f));
            }
        }    
    }
}
