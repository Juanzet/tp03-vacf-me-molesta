using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarInputHandler : MonoBehaviour
{
    CarC carC;
    void Awake()
    {
        carC = GetComponent<CarC>();
    }

     
    void Update()
    {
        Vector2 inputVector = Vector2.zero;

        inputVector.x = Input.GetAxis("Horizontal");
        inputVector.y = Input.GetAxis("Vertical");

        carC.SetInputVector(inputVector);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("AI"))
        {
            GameManager.Instance.CheckCars();
            Destroy(other.gameObject);
        }
        
        if (other.gameObject.CompareTag("AIPolice")) 
        {
            SceneManager.LoadScene("Gameplay01");
        }


    }
}
