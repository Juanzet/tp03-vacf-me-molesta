using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class CarC : MonoBehaviour
{
    [Header("Car Settings")]
   [SerializeField] float driftFactor = 0.95f;  
   [SerializeField] float accelerationFactor = 30f;
   [SerializeField] float turnFactor = 3.5f;
   [SerializeField] float maxSpeed = 20;

   [Header("Visual Settings")]
    [SerializeField] GameObject particSystem;

   //local variables
   float accelerationInput;
   float steeringInput;

   float rotationAngle;

   float velocityVsUp;

   //components
   Rigidbody2D rb;


   void Awake() 
   {
        rb = GetComponent<Rigidbody2D>(); 
   }

   void FixedUpdate() 
   {
        ApplyEngineForce();  
        ApplySteering();
        KillOrthogonalVelocity();
   }


    void ApplyEngineForce()
    {
        //Calculate how much "forward" we are going in terms of the direction of our velocity
        velocityVsUp = Vector2.Dot(transform.up, rb.velocity);

        //limit so we cannot go faster than the max speed in the "forwad" direction
        if(velocityVsUp > maxSpeed && accelerationInput > 0)
            return;

        //limit so we cannot go faster than the 50% of max speed in the "reverse" direction
        if(velocityVsUp < -maxSpeed * 0.5f && accelerationInput < 0)
            return;

        //limit so we cannot go faster in any direction while accelerating
        if(rb.velocity.sqrMagnitude > maxSpeed * maxSpeed && accelerationInput > 0)
            return;
        //apply drag if there is no accelerationInput so the car stops when the player lets go of the accelerator
        if(accelerationInput == 0)
            rb.drag = Mathf.Lerp(rb.drag, 3f, Time.fixedDeltaTime * 3);
        else rb.drag = 0;
         
        //create force for the engine
        Vector2 engineForceVector = transform.up * accelerationInput * accelerationFactor;

        //apply force and pushes the car forward
        rb.AddForce(engineForceVector, ForceMode2D.Force);
    }

    void ApplySteering()
    {
        //limit the cars ability to turn when moving slowly
        float minSpeedBeforeAllowTurningFactor = (rb.velocity.magnitude / 8);
        minSpeedBeforeAllowTurningFactor = Mathf.Clamp01(minSpeedBeforeAllowTurningFactor);

        //update the rotation angle based on input
        rotationAngle -= steeringInput * turnFactor * minSpeedBeforeAllowTurningFactor;

        //apply steering by rotation the car obj
        rb.MoveRotation(rotationAngle);
    }

    void KillOrthogonalVelocity()
    {
        Vector2 forwardVelocity = transform.up * Vector2.Dot(rb.velocity, transform.up);
        Vector2 rightVelocity = transform.right  * Vector2.Dot(rb.velocity, transform.right);

        rb.velocity = forwardVelocity + rightVelocity * driftFactor;
    }

    public void SetInputVector(Vector2 inputVector)
    {
        steeringInput = inputVector.x;
        accelerationInput = inputVector.y;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("AI")) 
        {
           //Destroy(other.gameObject); 
           //GameManager.Instance.CheckCars();
           Instantiate(particSystem, transform.position, Quaternion.identity); 
        }   
    }
}
