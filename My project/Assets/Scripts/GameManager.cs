using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance 
    {
        get
        {
            if(_instance is null)
                Debug.LogError("Game Manager is NULL");
            
            return _instance;
        }  
    }

    public GameObject police;
    void Awake() 
    {
        police.SetActive(false);
        _instance = this;
    }
    [SerializeField] int cars = 10;

    void Update() 
    {
        if(cars <= 9) 
        {
            police.SetActive(true);
        }
        if(cars <= 0)
        {
            SceneManager.LoadScene("Menu");
        }
        //Debug.Log(cars);
    }

    public void CheckCars()
    {
        Debug.Log(cars);
        cars--;
    }
}
